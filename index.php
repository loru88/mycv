<?php 

/* carico twig template system */
require_once 'vendor/autoload.php';


/**
* definisco i dati del template
*
*/
$lorenzo_datas = array(
	'name' => 'Lorenzo Adinolfi',
	'my_job' => 'Web Developer',
	'experiences' => array(
			'works' => array(
					'title' => 'Work Experiences',
					'list' => array(
								array(
									'company'=> 'Slamp S.p.A',
									'href'=> 'http://www.slamp.it',
									'when' => 'Ottobre 2013  now'
								),
								array(
									'company'=> 'Freelance',
									'href'=> 'http://www.lorenzoadinolfi.com/',
									'when' => 'Giugno 2011  now'
								)
						)
				),
			'education' => array(
					'title' => 'Education',
						'list' => array(
									array(
										'company'=> 'Ingegneria Informatica',
										'href'=> '',
										'when' => 'Ottobre 2017 - attualmente'
									),
									array(
										'company'=> 'Diploma di ragioneria<br>Istituto Tecnico Statale Commerciale "Gaetano Salvemini"',
										'href'=> 'latina',
										'when' => '2002 - 2007'
									)
							)
				),
			'skills' => array()
	)
);

$loader = new Twig_Loader_Filesystem('templates');

$twig = new Twig_Environment($loader, array(
    //'cache' => '/path/to/compilation_cache',
));


//render e visualizzazione template
echo $twig->render('index.twig', $lorenzo_datas);